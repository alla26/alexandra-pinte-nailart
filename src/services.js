import React from "react";
import "./services.css";
import { Link } from "react-router-dom";
import Header from "./Components/header";

class Services extends React.Component {
  render() {
    return (
      <div className="container-fluid pt-5">
        <div className="container">
          <Header />
          <div className="services-wrapper">
            <div className="row">
              <div className="col-8 offset-2 p-5">
                <ul>
                  <li className="d-flex justify-content-between">
                    <h4 className="services-title">
                      Manichiura cu oja semipermanenta
                    </h4>

                    <span className="services-price">60 RON</span>
                  </li>
                  <li className="d-flex justify-content-between">
                    <h4 className="services-title">
                      Protectie gel pe unghie naturala
                    </h4>

                    <span className="services-price">80 RON</span>
                  </li>
                  <li className="d-flex justify-content-between">
                    <h4 className="services-title">
                      Constructie gel (pana la marimea M)
                    </h4>

                    <span className="services-price">120 RON</span>
                  </li>
                  <li className="d-flex justify-content-between">
                    <h4 className="services-title">
                      Constructie gel (peste marimea M)
                    </h4>

                    <span className="services-price">150 RON</span>
                  </li>
                  <li className="d-flex justify-content-between">
                    <h4 className="services-title">Intretinere gel</h4>

                    <span className="services-price">90 RON</span>
                  </li>
                  <li className="d-flex justify-content-between">
                    <h4 className="services-title">
                      Demontat manichiura cu gel
                    </h4>

                    <span className="services-price">50 RON</span>
                  </li>
                  <li className="d-flex justify-content-between">
                    <h4 className="services-title">Reparat o unghie</h4>

                    <span className="services-price">10 RON</span>
                  </li>
                  <li className="d-flex justify-content-between">
                    <h4 className="services-title">French / Babyboomer</h4>

                    <span className="services-price">10 RON</span>
                  </li>
                  <li className="d-flex justify-content-between">
                    <h4 className="services-title">Design ({">"} 4 unghii)</h4>

                    <span className="services-price">5 RON / unghie</span>
                  </li>
                  <li className="d-flex justify-content-between">
                    <h4 className="services-title">MENicure</h4>

                    <span className="services-price">50 RON</span>
                  </li>
                </ul>
                <Link to="/appointment" className="btn btn-5">
                  Appointment
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Services;

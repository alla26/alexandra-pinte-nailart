import { APPOINTMENT_URL } from "./constants";

export async function getCurrentAppointments(days) {
  var url = new URL(`${APPOINTMENT_URL}/get`)
  const params = { days }
  url.search = new URLSearchParams(params).toString();
  const response = await fetch(url, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      'Access-Control-Allow-Origin':'*'
    },
  });
  return response;
}

export async function addNewAppointment(body) {
  const response = await fetch(`${APPOINTMENT_URL}/add`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      'Access-Control-Allow-Origin':'*'
    },
    body: JSON.stringify(body),
  });
  return response;
}

import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route, 
} from "react-router-dom";
import Homepage from "./homepage.js";
import Gallery from "./gallery.js";
import About from "./about_me.js";
import Services from "./services.js";
import Appointment from "./appointment.js";
import ContactMe from "./contact.js";

function App () {
  return <Router>
     <Switch>
        <Route path="/gallery3">
          <Gallery />
        </Route>
        <Route path="/about">
          <About />
        </Route>
        <Route path="/services">
          <Services />
        </Route>
        <Route path="/appointment">
          <Appointment />
        </Route>
        <Route path="/contact">
          <ContactMe />
        </Route>
        <Route path="/">
          <Homepage />
        </Route>
      </Switch>
  </Router>
}

export default App;


import React from "react";
//import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./about_me.css";
import Header from "./Components/header.js";

class About extends React.Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="container pt-4">
          <Header />
          <div className="about-me">
            <div className="ab-me-design"></div>
            <div className="row mb-5 align-items-center">
              <div className="col-6 align-items-center p-5">
                <img
                  src="images/text-about-me.jpg"
                  className="img-thumbnail about-img"
                />
              </div>
              <div className="col-6 pr-5 pt-3 mt-2">
                <h1 className="title-border">Few things</h1>
                <p className="description-wrapper">
                DESCRIERE SCURTA:
                -de genul iubesc cainii
                -fac unghii de cand e lumea
                -stiu sa citesc oamenii
                -abilitati de comunicare level 1000
                -stiu sa creez un mediu placut ptr cliente
                -am preturi de sparg piata si lucrez fain
                -pana acum am investit in mine cu urmatoarele cursuri: 1,2,3.....blah
                </p>


                <div className="container pt-4 skills-text">
                  <div className="row">
                    <div className="wrapper-box col-6">
                      <div className="row">
                        <div className="section-icon col-2"></div>
                        <div className="blah-wrapper col-10">
                          <h4 className="mb-2">Aptitude 1</h4>
                          <p>
                            Fugiat sit aute nisi duis.Proident sit esse labore
                            culpa culpa reprehenderit sint quis qui minim
                            consectetur.<br/>TEXT + STABILIRE ICONITA
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="wrapper-box col-6">
                      <div className="row">
                        <div className="section-icon1 col-2"></div>
                        <div className="blah-wrapper col-10">
                          <h4 className="mb-2">Aptitude 2</h4>
                          <p>
                            Fugiat sit aute nisi duis.Proident sit esse labore
                            culpa culpa reprehenderit sint quis qui minim
                            consectetur.<br/>TEXT + STABILIRE ICONITA
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="wrapper-box col-6">
                      <div className="row">
                        <div className="section-icon2 col-2"></div>
                        <div className="blah-wrapper col-10">
                          <h4 className="mb-2">Aptitude 3</h4>
                          <p>
                            Fugiat sit aute nisi duis.Proident sit esse labore
                            culpa culpa reprehenderit sint quis qui minim
                            consectetur.<br/>TEXT + STABILIRE ICONITA
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="wrapper-box col-6">
                      <div className="row">
                        <div className=" section-icon3 col-2"></div>
                        <div className="blah-wrapper col-10">
                          <h4 className="mb-2">Aptitude 4</h4>
                          <p>
                            Fugiat sit aute nisi duis.Proident sit esse labore
                            culpa culpa reprehenderit sint quis qui minim
                            consectetur.<br/>TEXT + STABILIRE ICONITA
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default About;

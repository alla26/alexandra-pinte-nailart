import React from 'react';
import { Link } from "react-router-dom";
import { Carousel } from 'react-responsive-carousel';
//import GoogleMapReact from 'google-map-react';
import MapContainer from './google-map';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import './homepage.css';
import Header from './Components/header';

class Homepage extends React.Component {
  constructor(props) {
    super(props);
    this.figureRef = React.createRef();
    this.state = {angle:0};
  }


  gallerySpin(sign) {
    let newAngle = 0;
    if (!sign) {
      newAngle = this.state.angle + 45;
    } else {
      newAngle = this.state.angle - 45;
    }
    this.figureRef.current.setAttribute("style","-webkit-transform: rotateY("+ newAngle +"deg); transform: rotateY("+ newAngle +"deg);");
    this.setState({
      angle:newAngle
    })
  }
	render() {
		return (
			<div className="container pt-5"> {/*toata pagina */}
        <div className="row">
          <Header />
          <div className="col-12">{/*body */}
            <div className="row my-5 align-items-center p-4 testing-background">
              <div className="col-12 col-md-8">
                <div className="jumbotron banner-nails">
                  <h1 className="display-4">Hello, world!</h1>
                  <p className="lead mb-2 pb-3">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
                  {/*<a className="btn btn-lg my-button" href="#" role="button">Learn more</a>*/}
                  <Link to="/appointment" className="btn btn-lg my-button">
                  Learn more
                  </Link>
                </div>
              </div>
              <div className="col-12 col-md-4">
                  <img className="card-image" src="images/pozaUnghii2.JPEG" />
              </div>
            </div>
            <div className="row mt-5">
              <div className="col-12 mb-3">
                <p className="h5 pre-headline">h2. Bootstrap heading</p>
                <p className="h1 headline">Bootstrap heading</p>
              </div>
              <div className="offset-2 col-8">
                <Carousel showIndicators={false} showArrows={true} infiniteLoop={true} showStatus={false}>
                  <div>
                    <blockquote class="blockquote text-center testimonial">
                      <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                      <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
                    </blockquote>
                  </div>
                  <div>
                    <blockquote class="blockquote text-center testimonial">
                      <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                      <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
                    </blockquote>
                  </div>
                  <div>
                    <blockquote class="blockquote text-center testimonial">
                      <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                      <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
                    </blockquote>
                  </div>
                </Carousel>
              </div>
            </div>
            <div className="row">
              <div className="col-12 p-0">
                <div id="gallery">
                  <figure id="spinner" ref={this.figureRef}>
                    <img src="images/pozaUnghii2.JPEG" alt/>
                    <img src="images/sample1.jpg" alt/>
                    <img src="images/image-49.jpg" alt/>
                    <img src="images/sample2.jpg" alt/>
                    <img src="images/about_me.jpg" alt/>
                    <img src="images/about_me2.jpg" alt/>
                    <img src="images/about_me_bottomImage.jpg" alt/>
                    <img src="images/slide-home2.jpg" alt/>
                  </figure>
                  <span className="controls left-arrow" onClick={() => this.gallerySpin('-')}>{"<"}</span>
                  <span className="controls right-arrow" onClick={() => this.gallerySpin('')}>{">"}</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 footer mt-5 p-2">
            <div className="row align-items-center">
              <div className="col-4">
                <MapContainer height="150px" width="300px" />
              </div>
              <div className="col-4">
                <p class="lead">
                  Tel: 0757 415 384  
                </p>
              </div>
              <div className="col-4">
                <p class="lead">
                  <a className="no-link" href="https://www.facebook.com/alexandrapinte.nailart/?ti=as">Facebook</a> 
                </p>
                <p class="lead">
                  <a className="no-link" href="https://www.instagram.com/alexandrapinte.nailart">Instagram</a>  
                </p>
              </div>
            </div>
          </div>
        </div>
			</div>
		);
	}
}

export default Homepage;


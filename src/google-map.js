import React from 'react';
//import { Map, GoogleApiWrapper} from 'google-maps-react';
import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Marker,
  } from "react-google-maps";

function MapContainer({ width, height }){
    const MapWithAMarker = withScriptjs(withGoogleMap(props =>
        <GoogleMap
          defaultZoom={7}
          defaultCenter={{ lat: 47, lng: 24 }}
        >
          <Marker
            position={{ lat: 46.7798611, lng: 23.6190096 }}
          />
        </GoogleMap>
      ));

      return(
        <MapWithAMarker
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyD25-jt1ASEZrQdbODesVqYzx2JMYE-k-8&v=3.exp&libraries=geometry,drawing,places"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: height, width: width }} />}
            mapElement={<div style={{ height: `100%` }} />}
        />
    );
}

export default MapContainer;
    
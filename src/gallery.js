import React, { useState } from "react";
//import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./gallery.css";
import Header from "./Components/header";
import GalleryModal from "./Components/gallery-modal/gallery-modal";

function Gallery() {
  const [modal, setModal] = useState({
    currentImage: null,
    isVisible: false,
  });

  const columnOne = [
    "images/gallery/received_318346389337673.jpeg",
    "images/gallery/received_279838783281940.jpeg",
    "images/gallery/received_378053263175826.jpeg",
    "images/gallery/received_275969233474690.jpeg",
    "images/sample1.jpg",
    "images/gallery/04.jpg",
    "images/gallery/05.jpg",
    "images/gallery/09.jpg",
    "images/gallery/received_327138824991488.jpeg",
    "images/gallery/06.jpg",
  ];

  const columnTwo = [
    "images/gallery/received_320285248996921.jpeg",
    "images/gallery/received_343944806599927.jpeg",
    "images/gallery/received_2715144375474630.jpeg",
    "images/gallery/received_574371536576526.jpeg",
    "images/gallery/03.jpg",
  ];

  const columnThree = [
    "images/gallery/received_305977690598592.jpeg",
    "images/gallery/received_3166517990074957.jpeg",
    "images/gallery/01.jpg",
    "images/gallery/02.jpg",
    "images/gallery/07.jpg",
    "images/gallery/10.jpg",
    "images/gallery/08.jpg",
    "images/gallery/received_285346239343905.jpeg",
    "images/gallery/received_205687284150981.jpeg",
    "images/gallery/11.jpg",
  ];

  const allImages = [...columnOne, ...columnTwo, ...columnThree];

  function closeModal() {
    setModal({
      currentImage: null,
      isVisible: false,
    });
  }

  function openModal(selectedImage) {
    setModal({
      currentImage: selectedImage,
      isVisible: true,
    });
  }
  return (
    <div className="container">
      <div className="row">
        <Header />
        <GalleryModal
          imageList={allImages}
          image={modal.currentImage}
          isVisible={modal.isVisible}
          onClose={closeModal}
        />
        <div className="col-3 px-0">
          {columnOne.map((image) => (
            <img
              key={image}
              className="gallery-image img-fluid"
              src={image}
              alt=""
              onClick={() => openModal(image)}
            />
          ))}
        </div>
        <div className="col-6 px-0">
          {columnTwo.map((image) => (
            <img
              key={image}
              className="gallery-image img-fluid"
              src={image}
              alt=""
              onClick={() => openModal(image)}
            />
          ))}
        </div>
        <div className="col-3 px-0">
          {columnTwo.map((image) => (
            <img
              key={image}
              className="gallery-image img-fluid"
              src={image}
              alt=""
              onClick={() => openModal(image)}
            />
          ))}
        </div>
      </div>
    </div>
  );
}

export default Gallery;

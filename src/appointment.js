import React, { useState } from "react";
import "./appointment.css";
import Header from "./Components/header";
import { AppointmentModal } from './Components/appointment-modal/appointment-modal'

function Appointment(){
    const [data, setData] = useState({
        firstName: '',
        lastName: '',
        phoneNumber: ''
    })

    function onChangeInput(event, label) {
        const value = event.currentTarget.value;
        setData({
          ...data,
          [label]: value,
        });
    }

    return (
      <div className="container-fluid pt-5">
          <div className="container">
              <Header />
              <div className="appointment-wrapper">
                  <div className="row">
                      <div className="col-8 offset-2 p-5">
                         <div className="form-row">
                             <label className="form-label">First Name:</label>
                             <input className="form-input" onChange={event => onChangeInput(event, 'firstName')} placeholder="" value={data.firstName} />
                         </div>
                         <div className="form-row">
                             <label className="form-label">Second Name:</label>
                             <input className="form-input" onChange={event => onChangeInput(event, 'lastName')} placeholder="" value={data.lastName} />
                         </div>
                         <div className="form-row">
                             <label className="form-label">Phone Number:</label>
                             <input className="form-input" onChange={event => onChangeInput(event, 'phoneNumber')} placeholder="" value={data.phoneNumber} />
                         </div>
                         <div className="event d-flex justify-align-between"><span className="event-text">It's a date!</span><AppointmentModal firstName={data.firstName} lastName={data.lastName} phoneNumber={data.phoneNumber} /></div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    );
}
export default Appointment;

import React, { useState, useEffect } from "react";
import { Close32, ArrowLeft32, ArrowRight32 } from "@carbon/icons-react";
import styles from "./gallery-modal.module.scss";

function GalleryModal({ isVisible, onClose, image, imageList }) {
  const [data, setData] = useState({
    currentImage: null,
  });

  useEffect(() => {
    setData({
      currentImage: image,
    });
  }, [image]);

  function goToNextImage() {
    const imageIndex = imageList.findIndex((img) => img === data.currentImage);
    const nextImage = imageList[imageIndex + 1]
      ? imageList[imageIndex + 1]
      : imageList[0];
    setData({
      currentImage: nextImage,
    });
  }

  function goToPrevImage() {
    const imageIndex = imageList.findIndex((img) => img === data.currentImage);
    const prevImage = imageList[imageIndex - 1]
      ? imageList[imageIndex - 1]
      : imageList[imageList.length - 1];
    setData({
      currentImage: prevImage,
    });
  }

  if (isVisible) {
    return (
      <div className={`${styles.container} py-5`}>
        <div className={`${styles.content} p-5`}>
          <button
            className={`${styles.close} d-flex justify-content-center align-items-center`}
            onClick={onClose}
          >
            <Close32 />
          </button>
          {data.currentImage && (
            <img className={styles.image} src={data.currentImage} alt="" />
          )}
          <button
            onClick={goToPrevImage}
            className={`${styles.arrow} ${styles.arrowLeft} d-flex justify-content-center align-items-center`}
          >
            <ArrowLeft32 />
          </button>
          <button
            onClick={goToNextImage}
            className={`${styles.arrow} ${styles.arrowRight} d-flex justify-content-center align-items-center`}
          >
            <ArrowRight32 />
          </button>
        </div>
      </div>
    );
  } else {
    return null;
  }
}

export default GalleryModal;

import React, { useState, useEffect, Fragment } from "react";
import { Modal } from "../modal/modal";
import DatePicker from "react-datepicker";
import styles from "./appointment-modal.module.scss";
import { Add20 } from "@carbon/icons-react";

import { addNewAppointment, getCurrentAppointments } from "../../utils/api";

const services = {
  service1: {
    duration: 60,
    label: 'Service 1'
  },
  service2: {
    duration: 120,
    label: 'Service 2'
  },
  service3: {
    duration: 90,
    label: 'Service 3'
  }
};

export function AppointmentModal({ firstName, lastName, phoneNumber }) {
  const [modal, setModal] = useState({
    isVisible: false,
    date: new Date(),
    service: "service1",
    disabledDates: null,
    errorMessage: "",
    successMessage: "",
  });

  useEffect(() => {
    let skipRequest = false;
    async function fetchDates() {
      try {
        const response = await getCurrentAppointments(14);
        const data = await response.json()
        if (!skipRequest) {
          setModal({
            ...modal,
            disabledDates: data,
          });
        }
      } catch (err) {
        console.log("Error: ", err);
      }
    }

    if (!modal.disabledDates) {
      fetchDates();
    }

    return () => {
      skipRequest = true;
    };
  }, [modal]);

  function openModal() {
    setModal({
      ...modal,
      isVisible: true,
    });
  }

  function closeModal() {
    setModal({
      isVisible: false,
      date: new Date(),
      service: "service1",
      disabledDates: null,
      errorMessage: "",
      successMessage: "",
    });
  }

  function onChange(ev, label) {
    const value = ev.currentTarget.value;
    setModal({
      ...modal,
      [label]: value,
    });
  }

  function onChangeDate(newDate) {
    setModal({
      ...modal,
      date: newDate,
    });
  }

  async function submitData() {
    try {
      const response = await addNewAppointment({
        start: modal.date.getTime(),
        summary: services[modal.service].label,
        duration: services[modal.service].duration,
        description: `<div><b>First Name: </b>${firstName}</div><div><b>Last Name: </b>${lastName}</div><div><b>Phone Number </b>${phoneNumber}</div>`
      });
      const data = await response.json()
      if (data && data.status === "ok") {
        setModal({
          ...modal,
          successMessage: "Appointment Registered!",
        });
      }
      if (data && data.status === "occupied") {
        setModal({
          ...modal,
          errorMessage: "Occupied! Please select another date and time.",
        });
      }
    } catch (err) {
      console.log("Error", err);
    }
  }

  function checkDisabledDates(date, disabledDates) {
    let disabled = false
    const dateNow = Date.now()
    if (disabledDates && disabledDates.length > 0) {
      for (let i = 0; i < disabledDates.length; i++) {
        const currentDate = disabledDates[i]
        const isBetween = date.getTime() >= dateNow && date.getTime() >= currentDate.start && date.getTime() <= currentDate.end
        if (isBetween) {
          disabled = true
          break
        }
      }
      return disabled ? styles.disabledDate : undefined
    }
    return undefined
  }

  return (
    <Fragment>
      <Modal
        isVisible={modal.isVisible}
        title="Add Appointment"
        onClose={closeModal}
      >
        {modal.errorMessage && (
          <div className={styles.formRow}>
            <div className={styles.successMessage}>{modal.errorMessage}</div>
          </div>
        )}
        {modal.successMessage && (
          <div className={styles.formRow}>
            <div className={styles.successMessage}>{modal.successMessage}</div>
          </div>
        )}
        <div className={styles.formRow}>
          <label>Select a service</label>
          <select
            className={styles.formField}
            value={modal.service}
            onChange={(ev) => onChange(ev, "service")}
          >
            <option value="service1">Service 1</option>
            <option value="service2">Service 2</option>
            <option value="service3">Service 3</option>
          </select>
        </div>
        <div className={styles.formRow}>
          <label>Pick a date: </label>
          <DatePicker
            className={styles.date}
            selected={modal.date}
            onChange={onChangeDate}
            showTimeSelect
            dateFormat="Pp"
            dayClassName={date => checkDisabledDates(date, modal.disabledDates)}
            timeClassName={date => checkDisabledDates(date, modal.disabledDates)}
          />
        </div>
        <button className={styles.submitButton} onClick={submitData}>Submit appointment</button>
      </Modal>
      <button className={styles.appointmentButton} onClick={openModal}>
        <Add20 />
      </button>
    </Fragment>
  );
}

import React from 'react';
import { Link } from "react-router-dom";
import "./header.css";

function Header() {
    return (
        <div className="header col-12">{/*header*/} 
        <ul className="navy">    
          <li className="active"><Link to="/">HOME</Link></li>
          <li><Link to="/about">ABOUT</Link></li>
          <li><Link to="/services">SERVICES</Link></li>
        </ul>
        <a className="logo" href="#"><img alt="logo" src="images/LOGO2sizedcopy.png"/></a>
        <ul className="navy">
          <li><Link to="/gallery3">GALLERY</Link></li>
          <li><Link to="/appointment">APPOINTMENT</Link></li>
          <li><Link to="/contact">Contact</Link></li>
        </ul>
      </div>
    );
}

export default Header;
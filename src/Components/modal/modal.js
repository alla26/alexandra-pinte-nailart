import React from "react";
import { Close16 } from "@carbon/icons-react";
import styles from "./modal.module.scss";

export function Modal({ children, title, isVisible, onClose }) {
  return (
    isVisible && (
      <div className={styles.overlay}>
        <div className={styles.modal}>
          <div className={styles.header}>
            {title}{" "}
            <span onClick={onClose} className={styles.closeButton}>
              <Close16 />
            </span>
          </div>
          <div className={styles.body}>{children}</div>
        </div>
      </div>
    )
  );
}
